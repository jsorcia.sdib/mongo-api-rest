
package org.utl.mongodb_empleado.controlador;

import com.google.gson.Gson;
import com.mongodb.Block;
import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import java.util.ArrayList;
import org.bson.Document;
import org.utl.mongodb_empleado.bd.Conexion;

/**
 *
 * @author Sorxia
 */
public class EmpleadoControlador {
    
    Conexion conexion = new Conexion();
    public void connectDatabase(){
        conexion.setMongoClient(new MongoClient("localhost", 27017));
        conexion.setMongodb(conexion.getMongoClient().getDatabase("neptuno"));
    }
    
    public ArrayList montrarEmpleados(){
        ArrayList documentos = new ArrayList();
        FindIterable<Document> iterable = conexion.getMongodb().getCollection("productos").find();
        iterable.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                documentos.add(document);
            }
        });
        return documentos;
    }
    
    public ArrayList busquedaEmpleados(String busqueda){
         ArrayList documentos = new ArrayList();
        FindIterable<Document> iterable = conexion.getMongodb().getCollection("pedidos").find(new Document("cuisine", busqueda));
        iterable.forEach(new Block<Document>() {
            @Override
            public void apply(final Document document) {
                documentos.add(document);
            }
        });
        return documentos;
    }
}
