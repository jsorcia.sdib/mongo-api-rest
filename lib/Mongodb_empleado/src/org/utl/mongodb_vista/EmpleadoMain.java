/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.utl.mongodb_vista;

import org.utl.mongodb_empleado.controlador.EmpleadoControlador;

/**
 *
 * @author Sorxia
 */
public class EmpleadoMain {
    public static void main(String[] args) {
        EmpleadoControlador ce = new EmpleadoControlador();
        ce.connectDatabase();
        ce.montrarEmpleados();
    }
}
