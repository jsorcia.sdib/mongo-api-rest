/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.utl.mongorest.rest;

/**
 *
 * @author Sorxia
 */
import java.util.ArrayList;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import com.google.gson.Gson;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import javax.ws.rs.QueryParam;
import org.utl.mongodb_empleado.controlador.EmpleadoControlador;
import org.utl.mongodb_empleado.modelo.Empleado;

@Path("empleado")
public class RESTEmpleado extends Application {

    final String FOTOGRAFIA_USER = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAecAAAHoCAYAAABzWyeBAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAzkUlEQVR42u3de2yc13nn8WfunCHnQpoc3SiREi3JpiSLvsSJUyeSmzgIkuyaTop1u0xpEigW7bZo6G0W2y6CjdxFt9ltG1PbokiLAqKEVfpHuhsa3TZdS7Zo+SLfZFOxTceSJQ2vkiiaV4nkzHCG+wdfKpREkTPDd2be857vBwhiy5RIPkPqx+e855zHMT8/LwCs49CRo7UiUrvMf9q/xj96XES6l/n17tbmpnEqD1iHg3AG8h62i6G6NHQbRCSy5NdrLPQhnzGCXEQkZvxPjGAfF5FYa3NTjFcWIJwBq4dvZEngLv7/Xpt/6hNLOvGuJZ35eGtzUzdfGQDhDOQ7gBe73qVB3CAiYapzR71G1700uFlCBwhnIOdOuMEI4wYR2UdV8hbaMSOw6bQBwhm4LYgX/7eXqhTNy4vdNYENwplwhj5BvNgJ76cjVsLiM+0uEelqbW7qoiQgnAF7hPH+Jf+roSrKO7MY1kZgj1MSEM6AtcM4IiKNhLGWYd1JZw3CGbBOIO9fEsg8L8bzS7rqbsoBwhkobHe8GMgcZcKd9IpIpxHUnZQDhDNgbiDXGmHcQneMHE0YHXWnLCyBj1MSEM4AgQxreZ6gBuEMEMggqAHCGcoEcsQIYwIZlgnq1uamDkoBwhk6hnKL0SU/QTVgQRNGN93BES0QzrB7IDeISJsRyuyyhip6RaTDCOoY5QDhDDsEckRYtoZ9PG+EdCelAOEMumSAbhqEM5BzKLcYoUyXDF0cFp5Ng3CGBQO5VhaWrdvokqF5N32And4gnFHsUG4wAvlpqgHcMCEi7SLSzrlpEM4oZCjvF5EDwjxkYDWHjW46RilAOCNfodwiPE8GcvG80Ul3UQoQzjAzlA8I85GBtXrZ6KQJaRDOIJQBQhqEMwhlAIQ0CGcQygAhDcIZhDKAfIR0W2tzUzelIJypgr6hvF84EgVYEUewCGfCWcNQbpCFSxIIZcDaDhohPU4pCGfYN5QjRihzoxegjglZOCN9gFIQzrBfMB8Q7r4GVNYrC8+jOykF4Qz1Q7nR6JbZ7AXYA5vGCGcoHMq1sjBvlufKgD3xPJpwhkKhHJGF5evvUw3A9iaMLrqDUhDOsG4wNwpL2ICOXhaRFo5eEc6wVijXGqH8BNUAtPasMEeacIYlgrlNFi4SYRc2AJGFXd0tXAVKOKM4odwgXCQC4M7YMKYwJyVQMpgPiMh7BDOAFXxHRLqNvSigc0aeu+UOEdlLNQBk4XlZWOqmi6ZzRp66ZYIZQLaeEJGYMewGdM4wIZRrhctEAJiHZ9GEM9YYzG3CTmwA5usVkUauACWckV0oR4xumXPLAPLpWaZdEc7ILJj3i0gn3TKAAuF2MQtiQ5i1gvmAiJwgmAEU0D7hyBWdM5YN5VqjW2YnNoBiOtja3NRGGQhngnnhp9UOumUAFnFGFjaLxShF8bCsXdxgPiAiPyWYAVjIXmGZm85Z01COyMIyNmeXAVgZy9yEszbB3CAiXXTLABTxsiwsc49TisJhWbuwwdwiC1dwEswAVLG4m7uBUhDOdgzmDhE5RCUAKKhGRLqMBgMFwLJ2/kM5IgvL2ByTAmAHPIcmnJUP5gZZ2PhVQzUA2AjPofOMZe38BXOj0TETzADsZp8sLHPXUgrCWaVgbhPOLwOwt8Xz0A2UgnBWIZg7ROQ5KgFAA2EReY+NYubjmbN5oRwRxjwC0Nczrc1N7ZSBcLZaMHcJO7IB6O1wa3MTXTThbIlgbhB2ZCNPXC6nBPz+m35temZGUqn0Tb/m9XjE5/Pe9GtT165TQBQloEWkjZ3chHOxg7lL2PiFDC2GaMDvF7fbLT6v90aolpYGxOVy5fX9x+MJicfjv/znRELm5uZkemZG4vGEJJJJXiSY4YyI7CegCediBPN+o2MmmLGsYFmpBPx+KQ0EpLQ0IIGAX4mPe3JySuLxhFyfnpbpmRk6cBDQhLMywdwiXMWJJVwupwTLyiQcDEooFFQmiDM1PT0jk5NTMjE1JeMTk7zgyFSvLFxW0k0pCGeCGQXh9XikPBKWivKIhEJBrT73sbFxmZickrHxCZbCsZoJo4MmoAlnghn565ArKyokWlVpu+54LV318NURGRkdvW2jGkBAE84EM/ImEg5JtPIuKS+PUIxVOupLV4Z5Tg0CmnAuSDC3i8h3qISeXfKG9etuO6aElcXjCRkYHJKxiQm6aRDQhHNegrlDRJ6mEnqF8vpoVDasj+b9aJPdpVIpuXR5WC4PDxPSWBrQja3NTV2UgnAmmEEoWyCkBy9dphhY1Nra3NRBGQhnghl3VFlRLtWbNrJ8nWeLy90jo2MUAyIiT7Y2N3VSBsKZYMZNAv4Sqdlcrd1RqGKbnJyS3v4BmZ6ZpRh64xk04ZxxMLcJIx+1sGnDeqnetIFCFNHA4CWWukFAE86rBnOLcFxKi265bmst55QtYnp6Rs5fjNFFE9AENOFMMOtqXVWl1NZsphAWk0qlpH9gSK5cHaEYBDQBTTgTzLpwuZxSV1vDJSIWd/Xqp9I7MMCxKwJa+4DWPpwJZvtjGVst09Mz8vG589zZrXdA1+o+zUrrcGYesx7BXH/PDs4tKyaVSknPL87yHFpf2o+b1DacCWb7q6wol7pttRRC4YCO9fZzJpqA1jKgnZoGcy3BTDDD2lwul9Rtq5XKinKKoae9ItKh6yevXTgfOnI0IiKdBDPBDDUQ0Fp7wrgUinDWQJfxExkIZhDQsL6nDx05eoBwtnfX3EEwE8wgoKGc7xsna7ShzYYwZjITzLCHj8+dl/GJSQqhp8d0GTWpReds/MRFMNtUwF/CrV8a2Va7RQL+Egqhp07jpA2dsw2CuUFE3uNr2p68Ho/ct/tezjFrJplMypkPerhJTE9aHLGydee85MgUbMjlcsrO7XUEs4Y8Ho/cs/1uCqGnvbJw4sbWbBvOHJmyv5rqaq7k1FhZWals3VJNIfS0z+5HrOzcOXcIO7Ntq7KiXKqq7qIQmotGq9jBra+n7byD25bhbJyJe4KvXXtiAxiW2rJ5k3g9Hgqhp0N23SBmu3A+dORoo4h8n69Z+6rbWstzZtzg8XhkW+0WCqGvLuMxJuFs4WBuEI3vYtXBpg3rec6M24TDIYnexfK2ri+/2HDjr23C2fjJqUPYAGZbAX+JVG/aQCGwrK1ba8XnZXlbU3vttkHMTp1zu7ABzNbqttZSBKyoiu5ZZ7baIGaLcD505GibiDzN16Z9rauqZDkbq9q0aZO4hItJNNZulw1iyoez8UI8x9ekfblcTtlcvZFCICP37dkt42OfUgg9hUWkww4bxJQO5yUXjcDGaqqr2Z2NjHl9PolWVMjM9DWKoae9svCYk3Auog4RqeFr0b4C/hIuG0HW7n/wARka6Jd4fJZi6En558/KhrNReC4asXvXvJnrGZGbLzz6qFw4f05Sc3MUQ09KP39WMpyNgrfztWdvwbJSCYWCFAI52bxli4TLyqS/v5di6CksCt97oWrn3CGcZ7a96o2cacbaPPTQQzI+NirDl4cohp72HjpyVMlGTrlwNgrNeWa6ZiCj7jlaWSlDQ4Ps4NbXdw4dObqfcM5vMO8Xke/wtUbXDGTTPYuI9PX1soNbX52qHa9SJpyXXM8JumYgq+45Eg5LOpWSWOwiG8T0pNzzZ5U65w7h2JQWqu7i6BTMtWdXvYiIxGdn5cL5cxRET08YUwsJZxO75kbh2JQWvB4P55phuvrde6Q0EBARkevXr0l/X4yi6EmZ28MsH84sZ+tlw7ooRUBe1G2ru/HPn45clZGrVyiKfsKiyK2SKnTOHcKxKW3QNSNfdu3addO/Dw0NskFMT/tUuD3M0uHMcrZeKivKuUMbeRMKh2Rbbe2Nf0+nUnLu3Fk2iOmp3erL25YNZ5azNeyaK+makV/btm696d/TqZScPfsRhdGP5XdvW7lzPiAsZ2vD6/FwfAp5V7d9+42NYYvis7MSu3ie4ujH0ru3LRnOXDZC1wzky+bqzbf92vjYKBvE9GTZ3dtW7Zw7+JohnIF8uLf+3mV/faC/T6YmximQXsKysEpLOGfQNR8QLhvRSsBfIj6fl0KgIKLRqETCyz8xuxi7wAxo/Vjy7m1LhfOhI0drReT7fK1o1jVzIxgKbMfd25f99XQqxQxoPbUTzivr4GtEP+XlEYqAgqq7++47/rf47CwzoPWz11i1JZyX6ZobRWQfXyN6YUkbxRAKh27btb3U+NioDA30Uyi9tBmrt4TzkmCOiAWXFVCArjlC14ziWG7X9lLDw5eZAa2XsJVyyCqdc5uwCUxLFSxpo0jutGt7KWZAa+cJq2wOK3o4swlMX16PRwIBP4VAUUSjUfF4PCu+DTOgtWSJ7tkKnXMHXwt6CgXLKAKKavOmTau+DTOgtbP30JGjbVqHs7F8wCYwbcOZ6zpRXJs2bsjo7ZgBrZ0Dxb45rNidM12zxioqIhQBRVW9OfOtLsyA1krRbw4rWjgb8zTZBKapgL+E8ZAoutWOVN2KGdBa+U4xj1YVJZw5OoVgGc+bYQ3rotGM35YZ0NopWk4Vq3NuE8ZBai3MeEhYRKbPnZcGNDOgtVG0o1UFD2eja27jNdcbs5thFdk8d17EDGitHNClc26na9ab1+PheTOs84NiOLTqeeflMANaG/uK0T0XNJyNh+tP81pr/pch55thMeuqqnL6fcyA1kaH3TvnA7zGyGZ3LFAIFRW5jy1lBrQWaowTRvYLZ7pmLOLKTlhNdfWmnH8vM6C1UdDm0mnXTwzWxWYwWM26devW9Pvjs7PSG2ODGN2zYuFM14wbXbO/hCLAcrw+35oft0xOTjIDmu5Zuc6ZrhlGOLOkDWsyY7Y4M6DpnpUJZ7pmLOXz+SgCLGktm8KWYgY03bMqnTNdM27gGBWsKlpVacqfs7BB7DwbxOierRvOdM2gc4YqgmHz7kZKJhPMgKZ7tnTnTNeMW8LZSxFgzc45iwEYmWAGNN2zJcOZrhm3dSZlpRQBlhYJm3uzMDOgba1N1c65jdcOS3GfNqzOm8Md26sZ6O9jg5g97c3nndt5CWdj8lQLrx2W4tpOWN369Rvy8ucyA9q2DqjWObcJk6dwC5+X582wtpI8bVhkBrRt7Tt05GiDSuFM14zbw5nNYLC4SpOOUy2HGdC21aZEOBs72Gp4vXCr0lKWtWHxHyBL8nu97PjYqAxfHqLQ9vK0sQHa8p1zG68VlsOGMFid2cepljM0NMgMaPtpsXQ4GzvX9vI64Vb52AUL5IOnAF+rzIC2HdObUrM75xZeIyyH581QRXk4/3tZmQFtO2GzLyUxLZy5dAQrYUkbuBkzoOmeC9U50zXjjjjjDFXk66zzcpgBbSumXkpCOANAETED2lZMy0FTwpnjU1i9c/ZTBCghHCr8WFNmQNvG08YNmZbpnOmasSKeOUMVwVDhLzdkBjTds+nhbGwE28frAQC5Ywa0bbRZpXNu47XAakKhIEWAEsLhSNHeNzOgbaHGjI1hZoRzI68FANv8IBkOFfX9MwPaFlqKGs6HjhxtFDaCAYCpmAGtvMa1bgxba+dM14xVcXUnkD1mQCstvNZ8zDmcjZ8KuBEMq+LqTqgmWllZ9I+BGdDKaylW50zXDAB5xAxope1byyjJtYRzG7UHgPxiBrTScm5icwpn46cBRkMiI1xAAqwNM6CVlXMTm2vn3EjNkSmGXkA1Xq/19klcjF1gB7d6ag4dOdpQyHBuoeYA7Kqi4i7LfUzpVEpisYvs4FZPTnmZdTizpA0AxcEMaCU1FqpzbqPWAFAczIBWTk5L27mEcyO1BoDiGR6+zBWfamnJazgbS9pc1wkARTY0NMgGMXVk3dQ68/0OAADmYwa0UrJe2s42nFuoMbJVGvBTBCAPmAGtlKzyM+NwZpc2csUlJED+XL9+jSs+1bA/X51zI7UFAOsZHxtlg5j17c3mru1swnk/tQWgg+rqTcp9zMyAVkLGTW5G4WyMh3yCugKAdTEDWrNwpmsGAOtbnAFNQFvWPqPZNS2cG6kpAFhffHZW+vt7KYR1ZdTs0jkDwC1Gro4o/fEzA9rSMmp2Vw1n4+A0t4IB0MZsPK7858AMaPt3znTNWJNUKkURgCJgBrQlZXRbGOGMvLs+PUMRgCJgBrS63XMm4cwRKgBQFDOgbRjOh44cpWsGAMUxA9p+nTPhDAA2wAxoSwmv1vwSzgCgCWZAq9M9rxbO+6gfAN1cuzZly8+LGdA2CGeeN8Ms16enKQIUC2f7dpfMgLaMfbl2zoQzTME5Z8BiPzAzA9oSVjrvTDgDgIaYAW0J+3MJ5wbqBkBHiWRSi8+TGdCKhbPRaoepG8wwPcMNYVCsq5yY0OZzZQZ0UTVk2znTNcM0qVSaIgAWxQzooqo5dORobTbhvJ+aAdBRwgYTqbLFDGjrdc90ziiIaYZfQBFXrui5SYoZ0EWzP5tw3ku9YKY5lswAyxsaGpTxsU8phBU7Zy4fAaCzqckJrT//vr5ednAX1r5MO+cGagWzTU7xzQ41TEzq/bXKDOjCW+4yEsIZAJZIJBLa14AZ0AVHOKM4uF8bqhgd5ZmryMIM6P6+GIUojNpMwpnNYDCVy+UUn9dLIQDFfDpylSs+C2P/rb/gXvovK13CDWTL6/FI9cYNUlV1F8WAMp785jdleHhYznR3y4UYnePQ0KCUlpaKP1DGF0f+NKzWORPOMEVlRbncv3c3wQwlRaNRefwrX5HfeOrXJRLW+yZjZkAXRPjQkaORlcK5lhphrTZtWC912/hSgvpC4ZA82diofUAnkwk5e/YjviAK2D3fGs77qQ/WIhIOSfWmDRQCtuH1+eTJxkYpDQS0rkN8dpYZ0EUMZ9od5Mzlckrtls0UArYM6Aca2CvLDOi8ql0pnGuoD3JVHg6Lz8eubNhT/e492nfPIsyALnjnzLWdWKsN69dRBNha3bY6iiALM6Dj8VkKUYhwFpEItUGuXC6nBAJ+CgFbq67eRBFkcQf3OXZwmyt8p3BuoDbIVcBPMMP+Nm/ZQhEMzIA239IV7KXhXEtpkHvn7KIIgGaYAW26COEMU7FRBrrweDwUYQlmQJuqYblwbqAuAIBsMQPaNLXLhXOYuiBXc2wMgSaSySRFuAUzoPMUzgy8wFpNz8xQBEBjzIDOT+ccoSZYi1QqRRFge5MTkxRhpfowA3qtam4NZzpnrLFz5kIC2N/VYa6uXA0zoNfm0JGjtXTOMDegp1nahr0NXx2hCBkYGhpkg1jubgrnWuqBtZqcnKIIsLW+/j6KkAFmQK9JhHCGqa5+yllH2FciHpfxiQkKkSFmQOesYWk4A2s2PTMr8XiCQsCWPvzgQ4qQJWZA524xnPdRCphhYJCr/GDTcP6ohyLkgBnQdM6wgJHRMbpn2E7PB+/L9elpCpHrD+39fTI1MU4hMhMREXEeOnI0Qi1gpvMXYxQBtpGIx+WNt9+hEGt0MXaBGdBZcApnnGGyqWvX5fLlYQoBWzh27BhXdpqAGdAZ27cYzoDpegcGOfcM5b13+l0ZGGIfhVmYAZ1d5xyhDMiHno/PcvYZ6n79fvC+vHWa5WyzjY+NytBAP4XIIJwbKAPyIZVKy0dnP2GJG8p5uetleeX1UxQiT4aHLzMDegWHjhxtcFMG5FvvwKCMjo9L9cYNEgoFKQgs3S2/232GndkF0NfXKz6fT/yBMopxuwjhjIKYunZdPjr7iXg9HimPhKXE55NAwC+pVErKyyMUCAV3/tw58fp8MnJ1RCanpqR/oJ9QLqDFGdA7dtwrLjdRdCu38MwZBZRIJuXKLcMDdtRtJaBR8GA+fuIEhSiy+OysXDh/TrbvvJdi3IJnzii6CTaNocAuXLxIESzi+vVrzIC+XQNHqVB0Y+MME0Bh9Q8OUgQLYQb0bSKEM4oukUxy5AoFc/7cOS4VsSBmQN+McIYlXB3hWAUKgyVta0qnUnLu3FluECOcYSVjzMlFASTicZa0LR7QzID+ZTg3UAYUWyqVlrGxcQqBvPrk3FmWtC2OGdC/DOcwXw6wgmGWtpFnF2Pc66wCZkDLfpa1YZ1vyIlJSaVSFAJ5MTkxyRALheg+A5pwhqVcvUr3jPz48MMPKYJidJ4BTTjDUi5dYUgG8uP8BZ5jqkbnGdCEMyyFM8/ISzCfO8e92YrSdQY04Qy6Z9jez99/nyIoTMcZ0IQzrPeNODEp8XiCQsAUkxOTMjwyQiEUp9sMaMIZ1uyeL3PPLszx5ptvUASb6Ovr1eaKT8IZljQyOsqxKqwZN4LZy+IMaB02iBHOsKRUKi2XLvPsGWtz+vS73AhmM4szoAlnoEgYhoG1ds0fffwLCmFDOsyAJpxh3b9ck0kuJUHOPvzgQ7pmG7P7DGjCGZY2MHSJIiC3cP6ohyLYnJ1nQBPOoHuG7fR88D6XjmjAzjOgCWfQPcNeP9DF4/Ju9xkKoVFA23EGNOEMumfYyunT79I1a8aGM6DHnSLCgFNYXu/AAOeekVHXzA5tPY2PjcrwZduMBO12ikiMlxVWx7lnZNo1s0NbX0NDg7aZAc2yNpRxeXiY7hl3NDkxKT//gAEXurPLDGjCGUp1z7HefgqBZb3yykmKANvMgCacoZSR0THmPeM2/X19MjA0RCEgIgsbxHpjam8QI5yhHI5W4VavnzpFEXCTyclJlWdAx5wi0sXLCJVMXbsul9kcBsN7p9+V8YkJCoHbKDwDOkbnDDW750uX2BwGmZyYlPd+zoUjuDNVZ0ATzlBSKpWWTy7EKITmXnnlJEensKKFDWLnldsgRjhDWeMTkzI2Nk4hNHX+3Dk2gSEjyWRCtRnQPHOG4n9Bx3pZ3tZQIh6Xl199lUIgYyrNgG5tbuKZM9TG8raejh07xnI2sqbSDGjCGcpjeVsv751+l+Vs5Gygv8/qG8QmREScrc1NXbxcUN35WK/E4wkKYXPDw8PszsaaWXwGdDedM2wjlUrL2U/OUwibO3HiBMvZWDMVZkA7l7bRgMqmZ2a5e9vGXu56mctGYBoLz4COLQ3nbl4q2MGVqyM8f7ahng/el1+c/ZhCwFQWnQF9UzgDtnE+1ivT0zMUwiaGh4fljbffoRDIC6vOgKZzhu2kUmk5fzHG+WcbSMTj8sILL/CcGXllsRnQXUvDeZyXB3YyPTMrH59jg5jqftrZKdenpykE8sqKM6AXwznGywO7mbp2Xc5zQYmyjr3wAhvAUDAWmgHdTTjD9kZGx2RgkPnPqnm562W5EOOvJRSWFWZAtzY3jRPO0MLgpcty9eqnFEIR7MxGMRV5BvSNG3acRlITzrC1C719BLQiwfzK66coBIqqiDOgx28KZ0MvLwkIaBDM0F0RZ0B3LxfOdM8goEEwA1K0GdDLds7dvBwgoEEwAwuuX79W6Cs+uxb/wb1cYgM6BHQ8kZDqTRsoRhEde+EFdmXD0sbHRmWkrEwqq9YVrXPu4mWATgYvXWZQRhFxXAqqKNQM6Nbmpu7lwpnOGdqZnuEO7mIZHeXRAtRRgBnQN23Kdi6X2AAA4JcWZ0DnMaBjy4az4QwvAQAAt4vPzkp/f95OHXetFM4xyg8AwPLyOAN6xc65m9IDAHBneZoBTTgDALAWF2MXTN3B3drc1EU4AwCwBulUSmKxi2ZtELvtQbbzluSOUXIAhXB9epoiQGkmzoDuXjGcDS9TcgCEM7A6k2ZAZxTO3ZQbAIDMDA9flpGrVwhnAACsZGhocC0bxAhnAADMtoYZ0BPL7fe6LZy5xhMAgOzlOAO6a7lfdN7hjdkUBgBAlnKYAd2dTTjTPQMAkIPxsdFsNogt2zm7V3jj71BiWIXL5ZSA33/j30PBYFa/f3Jq6sY/T8/MSCqVpqgA8magv09KS0vFHyjLqXN20znDSuHr83rF5/NJacAvLpdLSksD4nK5THgPG277lXg8IalUiuIXyde++tVV3yYRj8vw1ZHbfv3y5Us3/nlsYkKSySQFheWcO3dWdu26T1zuO0WtnGltbhpf7j845ufnl/0dh44cjYlIDeWF2bwejwQCfikNBCQULBOfzyc+n5fCYM36+/oWupaBwRshnkgmZXxiguKgKHwlJbJjx713CujDrc1NLdl0zovdM+EMU8I4FCyTUDAooVCQIEbebN6y5ab/X2pyYlImJsZlYGBQrl2bktGxMUIbebc4A7p2a91y/7nrTr9vpXDuEpEnKC1yEQmHJBwMSnl5hDCGJYTCIQmFQ7cFd39fn4xcHZGhS0MyNj7OtaIw3fjYqAz7/RJdv9G0cAYy4nI5pTwcloryiIRCQZOeEwOF6bY3b9ki98sDNzrsgf5eGRy6JFeGhwlrmGJoaFD8/oAEw5HFX5pYadjUHZ85i4gcOnJ0XETClBUrdcgVkYhUVd1FMWBLw8PDMtg/ILHemAyPjFAQ5Mzpcsn27TsWd3A/39rc1JhL57zYPbO0jZt4PR7ZsC7KkjW0EI1GJRqNyv0PPiCJeFw+OXdWLsZ6ZWBoiOIgK4szoI0NYl0rvS3hjKy65GjlXVJeHqEY0PMHU59P6nfvkfrdewhq5GRxBvS2u3euOZyhucqKcqnetJEuGbhDUE9OTMqHH34o5y+c5xk1VjU3Nze72hyLFZ85i/DcWVcul1MqKypkw/p1hDKQhf6+Pvn5z39ON407CoUjH/3XZw/Ur6VzFhHpFJGnKadenXJtzWZ2XAM5WNz9PTkxKe+9956cv3iBG8xwc/PjdL2+2ttkEs5dhLM+oczyNWBWdxSSffv3ySOPfE5On36XJW/cMD+f/j+rvU0my9q1InKRctpXsKxUqjdukFAoSDGAPOr54H15t/sMIa0xp9OZeu6HP1y1MXau9gbGIekzlNR+XC6nbKvZIvX37CCYgQKo371Hvv3tb8vDDz4kHo+HgmjI6/P1ZvJ27gz/vC4R2UtZ7YPnykDx3P/gA7Jr9y45ffpd+ejjX/BMWqfO2eH8WUZvl+Gf10lJbfJTm8cj9+64W+q21RLMQHE7KHnk84/Ir33zW7KttpaCaGJ6+vrfZfJ2qz5zXsSRKvWtq6qUzdUbCWXAgvr7+uT1U6eYlGXnrtnpTD73wx9mtOPWmcWfS/esKJfLKTvqtrKMDVjY5i1b5KmnnpL7du/hebRNebzetzIO8iz+3C5Kq56Av0T21N/LlZuAIhaXuqOVlRTDZlJzcz/ORzjTOStmXVWl7Nl1L+eWAcWEwiF58pvfZFe3zcxlEc4ZP3MWETl05GinMAjD8lwup9RUVzPGEbCByYlJ+dm//Ixn0Ypzu91Df/Hnf74pH50z3bMiwVy/cwfBDNioi37qqafknh07KYbKHI6fZPPmhLONBPwlcv99uyUQ8FMMwGb27d8nX37sMZa5FTWXTHZkleXZLGuLiBw6crRLRPZRamuJhENyN2eXAdtjmVs9Tpdr7Lm/+IuKfHbOIiIdlNpaKivKZef2OoIZ0EAoHJInGxu5uESlcHY4OrP+PTm8n05Kba1grtvGNymgE6/PJ49/5Sty3+49FEMBc3NzR7L9PVkva4uwa5tgBmAVPR+8L6+8fopCWJTD4Zhtf+65rDcCOXN8f3TPBDMAC6jfvUeebGxko5hFOZ3Of8rp9xHOBDMAtUWjUfnG179OQFtQKpX6q5w67lyWtUVY2iaYAVjN8PCwnDhxgp3c1umaP33uhz/M6R5W5xrebwelJ5gBWKuDfrKxUSJhBghaxE9y/Y05h3Nrc1OniPDjWYFEwiGCGcCqvD4fAW0R6XT6bwoeznTPhRPwl8jdBDMAAloZTqfz6sH29m7C2aZcLqfU37ODC0YAZB3Qj3HdZzG75v+2pnBfy29ubW7qFpEzvAx5DOadBDOA3LCLu6g6ixbOhnZeg/yoqa5miAUAAloxTqfz2MH29lixw7mTl8J866oqGfsIwLSA/txnHqIQBZJOp3+85oBf6x/Q2tw0LiKHeTnMEywrldqazRQCgGnqd++Rhx8koPPN4XDMHGxv7yh6OBs6eEnM4XI5Zef2OgoBwHT3P/gA06zybH5+/m/N+HNMCefW5qYuYWOYKXbUbWMDGIC8efwrX+GIVX61WyaczfyAdLauqlJCoSCFAJBXDMrID6fT+eZaN4LlI5w7hRvDchbwl/CcGUBBeH0++cbXv04hTJZOp39kWtCb9QcZG8M6eHlyU7e1liIAKJhoNMoGMRM5nM4rZmwEy0fnLMLSdk42bVjPeWYABXf/gw9I9caNFMIE8yZ2zaaHc2tzU0xEnudlylzAXyLVmzZQCABF8fjjj/P82YLNqdPqH6DdsZwNoJi8Pp88/qUvUYg1cDidf3+wvX3c0uHMsarMsZwNwAo2b9ki9+zYSSFyNJ9O/w+z/0xnnj5WuufVflr1eGTD+iiFAGAJjzzyOSkNBChEtiG6cHyqW4lwbm1u6hCRXl62O6vdUs1lIwCs0zD4fLLvi1+kEFlKp9M/yEvo5/Fjpnu+g2BZqZSXRygEAEtheTs7Dodj8GB7e6dq4dwhXEpyh66Zy0YAWNMjj3yO3dsZmp+f/16+/uy8hbNxKQnd8y0qK8rZBAbAsrw+n+x79FEKsVrXbPKlI4XsnMUIZ7png8vl5IpOAJZXt327RCsrKcRKXXOenjUXJJzpnm+2PhplExgAJXzpS1+mCHfqmh2O65Ln66qdBfg86J6NrpmjUwBUEQqH2Bx2p655fv7Pzb50pODhTPdM1wxATWwOu2PXnPdMcxbo89G6e6ZrBqAir88n9+68h0IUuGsuWDjr3j3TNQNQtnv+/CPcHFbgrrmQnbO23TNdMwDVPdCwlyIUsGsuaDjr2j3TNQNQXf3uPdp3z4XsmgvdOWvXPdM1A6B7tk3X/L1Cdc0FD2fduufycJiuGQDds/pd8+DB9vaCZlehO2dpbW46IJpMrKretJHvaAB0zzbomgv9Pp1F+lwP2P3FjIRD4vN5+W4GQPesftfcoUU4G/Oez9j5Bd2wjmfNAOiebdA1f7sY79dZxM+5za4vptfjkVAoyHcxANu5e/sObW4Nczgcrx1sb+/SKpxbm5u6ROR5umYAUKj50OjWsPn5+d8r1vt2Fvlzt2X3XFV1F9/BAGxr165dOnyahw+2t3drGc6tzU0xETlop1ezsqKc41MAbC0UDsm22lrbfn7GhSNFbR6dFqjDAbHRxSRVlXTNAOzvnnvsu7RdyGs6LRvOxsUkbXZ4QdkIBkAXm7dsseWxKuPo1IFifxxW6JwXj1a9TNcMAOrYdW+9Hbvm37PCx+G0UE2U754JZwBahfNue20MM45OdRLON3fP3aLw5rCAv4QbwQBoxevz2WpjWLEuHLF65yyi8OawqrvomgHoZ9vWrXb5VJ492N4eI5yX757HRaRFxVe1vDzCdykA7dRt3678xjCrbAKzcucsrc1NnaLY5jCWtAHobHP1ZqU/fistZ1s2nA0totDyNkvaAHR2b/29Kn/4h4t1f7Zy4WzcHHZAlVeWJW0AOotGo2oubTsc18SiJ4Ws2jlLa3NTuygwVpIlbQBQdGl7fv43i30TmHLhbGixfNccoWsGAAWXtp+3yplm5cLZOPv8rJU/xgqWtAFAraXtheVsSzd/Vu+cpbW56YBYdHnb6/FIIODnuxIARKGlbQsvZysTzgZL/oQTCpbx3QgAhupNG1X4MC29nK1UOBvL289Y7eNiSRsAfqlu+3bxeDzW/QAVWM5WrXNe3L1tqctJGA8JADdbV1Vl3Q9OgeVs5cLZ0CIWuZwk4C8Rl8vFdyIALLG1tsaqH5oSy9lKhrNxOUmbFT4WjlABwO2qN1svnB0Ox6AoNrdBtc5ZWpubOkTk+WJ/HGwGA4Bl/m4Mhyx3pGp+fv7bqixnKxvOhhYR6S3qFyDPmwFgWeuiUSt9OM9a8e5sW4azMVqysVjvP1hWyncfANyBVWY8OxyOD6w2CtLunXNRbw8LBemaAeBOqqLrrJDM1+bn5/+VqjV0qvwFYNweVvDjVTxvBoAV/o60wnPnhWNTMcK5eBqlwMerSksDfPcBwAqK/Nz5oErHpmwZzsbz5/2Fen+cbwaA1W3auKEo79d4ztymev3s0DkX9HrPYBlL2gCwmspiPHdW/Dmz7cLZCOh2KcD5Z2VGogFAEUWj0cLfs634c2ZbhrOhRfI8XpLnzQCQmfJwuJDv7lnVnzPbNpyN588tkscNYsxvBoDMrF9fsOfOz6t6nlmXznnx+XNLPv5sLh8BgMxFqyrz/j4cDscHoti92VqGsxHQnZKHC0oCfrpmAMi4ocn3svbCBrDfVO3ebG3D2QjoAyJy2Mw/k81gAJBF55zvs84Lwdxtx9o5bf610SYmbhBjMxgAZBnQlXlb2n7GThvAtArnJReUmLJBjM1gAJCdsvzcDXH4YHt7u53rZvfO2bSADvhL+C4DgKzD2dxBQcYNYC12r5tThy8OM3Zwe71evssAIEvV1ZvMDOaP5ufnv6BD3Zy6fIEYO7hbc/39bAYDgOz5SkxadVzYmf1v7bgzW+twNgK6Q3LcwV3K82YAyJoZO7YdDse0zM9/wa47s7UPZyOgW3IJaJ/Px3cZAOTU3Kxt5XF+fv53dQpmLcN5SUBndcSKndoAUJRwbj3Y3t6hW82cGn+97M80oNmpDQC5q6i4K9ffelDHYNY6nJccsVo1oF0uF99dAJCjHE+7HD7Y3t6ma8107pwznmIVCgb57gKAHIVDWV9EcliHs8yE88oB3S2rXFLipnMGgJwFQ5kPwNDlkhHC2YSAZjMYAOQu07PODofjA10uGSGcTQhot9tNgQAgR5mcdV4MZl0uGSGccwtoOmcAKBCCmXDONKBvXPPpclEiAFgrj8dDMBPOaw7ojsWADvjpmgFgrcrDYYKZcDY3oAEA5iKYCec1BXR5OPyXVAIACGbC2UK+9KXHfn98fOK/UwkAIJgJZwv5/Ocf+cOpqWvfpxIAkJuysjKCmXA232c/+/AfE9AAkJuKiophgplwJqABwCJGR0evz6VSDxPMmXPMz89ThSy9+eZb/yUYLHuWSgDAyibGJ8bDkfDW+vp6gplwzr/33utu8njch51OJ1MxAGAZkxOTo6FwqI5gJpwL6q233v5CIOA/QUADwM2uTV27UhYsu4dgzg3PnNfg4Yc/88r09Mxj6XQ6QTUAYMHMzEw/wUznXHQ9PT218XjitM/nraAaAHQ2NTX1cTAY/BzBTDhbJaAjyWTyQ4/Hs5FqANDRtalr//jwZx/+11SCcLacd945/Wog4P8VKgGAYEaueOZssoceevDRmZnZv6ESAHQxMzPznwlmOmclnH7n9Hf9Af+fUQkANtdaX1/fQRkIZ2UYR61edDqdHqoBwE5Sc3OzLrf7kfr6+m6qYT6WtfPo4Yc/84rT6dyRSCRGqAYAu0jEE+cJZsJZafX19TGv17t9enrmNaoBQHUzMzNdXp/3IYI5v1jWLqC3337nz0pLA9+lEgBUlEwmf7R3797foRKEs+2cPv3uN3w+70+dTqebagBQQWpuLu5yu3+bjV+Es6319PREZmfj3SUlvhqqAcDK5ubmrrjd7q+yjE0469RF/4PfX/ItKgHAihLxxDGvz/tvuIqTcNbOW2+9/e8CAf9fM9kKgMU8W19ff4AyEM7a6unpqZ2djXexzA2g2NKp1LTT5fp6fX19F9UgnCEib7/9TkdpaeBpKgGgGJLJ5Jsej+erLGMTzrjF6dPvfsPtdv3E4/GUUA0ABfRMfX19O2UgnHEHPT09kZmZ2Vf8/pLdVANAPiXiifNen/fX2I1NOCNDr79+6plQKPhnbBYDkA/JZPJHHo/nj1jGJpyRQxfNmWgAZjI2fTXV19d3Ug3CGWvwxptv/VVZIPA7TpeT+9AB5Iyzy4QzTPbaa6fuKynx/bPfX7KJagDIhjHi8Tfolgln5Mnrr5/6QSgY/I900QDolglnWMixYy/WlJdH/l8g4N9JNQDQLRPOsFYX/UwgEPiB1+vxUg0Ai9iJTTijyHp6eiKTk1PPh0LBL1INQG+zs7OxkpKSVq7fJJxhESdPvvq1QMB/OBDwV1INQC+publ4KpVqb7j//j+kGoQzLIgNY4BeZmZmuvx+f2t9fX2MahDOsLCenp7I+MTkv0TCoc9SDcCeEvHEp8bVm11Ug3CGQo4ff+nR8kj4H/wB/zqqAdhDam4unkgmn33wwQf/lGoQzlDYiRNdbRUV5X/KtCtAbTMzM//b7/f/FruwCWfYyKuvvv7jSDj0FM+jAeVC+Yzf72/kuTLhDJvq6emJjI6OdYbDoS+6XC4HFQGsa2pq6uNgMPjbPFcmnKGJ1147dZ/b7ToSDJbtpRqAtRibvb5bX1/fQTUIZ2jo+PGXHg2UBjoi4VAd1QCKa3Z2djI1N/dHn3n44b+mGoQzQEgDRZSam4vPzs7+B0IZhDOW9fqpN37d5/G0c/wKKEwop1Kpdq/P9wN2YINwBp00UESLy9elZWU/JpRBOIOQBoooEU+Men3eP2CjFwhnmBbSZWWlf1taGriHI1hAdqanpwcDgcD3CGUQzsiLY8derPH7Sw5zThpYnXF5SBvnlEE4o2C6uk7+XSQSftrr9bipBrAgNTeXSCST/+j3+7/LjV4gnFE0J050tQUCgf8UDJatpxrQVSKeGPP6vH8sIh1s8gLhDMs4fvylR/1+/1+GQmV7WfKGLuKz8RO+Et//rK+v76QaIJxhaV1dJ/8uFAo+VVLiK6MasJvUXCrucrt+JCLtLF2DcIZyTp589Wsul+tP6KZBlwwQzrBoN+33l3ydZ9NQyVxybsTtcf+JiHTSJYNwhm0dP/7So16v9w9CoeDXvF6Pl4rAatLp9KzT6fwbWdjc1U1FQDhDKy++eOKpkpKSPywtDezmSBaKHMjxdDr9f91u9/9i2RqEM7AkqEtLA98vKy3d6XQ5nVQEBDIIZ8BCTp589bdcLtfv0lHD9ECen59Np1L/RCCDcAbW2FH7A/5nfF7vfX5/iZ+KIFupVGosnU53ejyeI1ylCcIZMNnx4y89WlJS8u89Hvdj7PrGSpLJ5IcOh+Oo2+3+e3ZZg3AGCujkyVe/53K5vlVaGqhn57f2YTyUTs93+Xzen7BcDcIZsIjXXjt1XzKZ/H2vz7s/4PfX8Kza3ubm5qaSybl3/P6SIyLSRXcMwhlQwPHjLz3qdrtbjLDeTGettkQieSWZTL7t9Xp+5vF4/pkwBuEM2KazTvyG1+d7zON27ywtDUSoijWl0+lkIpEcSqVSXaWlgQ4R6WbSEwhnQBMvvnjiKZ/P94Tb7WpwuVwbCOyiBPFcIpEcTCaTbzgcjpfKykpfoCsG4QzgtsD2er2/4nDIfSUlJbvcbneE59fmmJmeuTKXSl12Op3dTqfzH/z+klfpiAHCGcjZiRNdbW63Z5vDIfd5PJ6dHo+njJGYt5udjV+bn5+fiCcSA06n8510Ov1qJBx6g24YIJyBgunp6YlcuTLc4nA41rnd7kecLleJx+3eKSJix2Xy69enxx0Ox1wimTw/n54fdzike24u9UllZcVxAhggnAFlvPbaqfsSifivioh4PN6HReY3ioh4vd5tDocjuPh2/pKSUCHvFZ+ZmZ1Jp9PxxX9Pzs19nE6lZkVEnE7X+Xh89n0RkXXrooxLBAhnAEudPPnqb6VSczkvoadS6Xe+/OVffZVKAtb3/wEcT/IxmB7QxgAAABl0RVh0U29mdHdhcmUAQWRvYmUgSW1hZ2VSZWFkeXHJZTwAAAAASUVORK5CYII=";

    @GET
    @Path("getAll")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAll() {
        String out = null;
        EmpleadoControlador ce = new EmpleadoControlador();
        try {
            ce.connectDatabase();
            ArrayList datos = ce.montrarEmpleados();
            Gson gson = new Gson();
            out = gson.toJson(datos);
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }

    @GET
    @Path("serch")
    @Produces(MediaType.APPLICATION_JSON)
    public Response serch(
            @QueryParam("buscar") @DefaultValue("1") String busqueda
    ) {
        String out = null;
        EmpleadoControlador ce = new EmpleadoControlador();
        try {
            ce.connectDatabase();
            ArrayList datos = ce.busquedaEmpleados(busqueda);
            Gson gson = new Gson();
            out = gson.toJson(datos);
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }

    @POST
    @Path("insert")
    @Produces("application/json")
    public Response insert(
            @FormParam("nombre") @DefaultValue("ert") String nom,
            @FormParam("apellidopa") @DefaultValue("1") String apellidopa,
            @FormParam("apellidoma") @DefaultValue("1") String apellidoma,
            @FormParam("fecha") @DefaultValue("1") String fecha,
            @FormParam("genero") @DefaultValue("1") String genero,
            @FormParam("email") @DefaultValue("1") String email,
            @FormParam("telefono") @DefaultValue("1") String telefono,
            @FormParam("rol") int rol,
            @FormParam("nombreUsuario") @DefaultValue("1") String usuario,
            @FormParam("contrasena") @DefaultValue("1") String contrasena,
            @FormParam("foto") @DefaultValue(FOTOGRAFIA_USER) String foto
    ) {

        String timeStamp = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
        Empleado em = new Empleado();
        EmpleadoControlador ce = new EmpleadoControlador();
        ce.connectDatabase();
        if (foto.equals("")) {
            em.setFoto(FOTOGRAFIA_USER);
        } else {
            em.setFoto(foto);
        }
        em.setNombre(nom);
        em.setApellidoMa(apellidoma);
        em.setApellidoPa(apellidopa);
        em.setFechaNacimiento(fecha);
        em.setGenero(genero);
        em.setEmail(email);
        em.setTelefono(telefono);
        em.setContrasena(contrasena);
        em.setNombreUsuario(usuario);
        em.setEstatus("Activo");
        em.setRol(rol);
        em.setFechaRegistro(timeStamp);
        em.setClaveAcceso(contrasena);
        String out = null;
        try {
            ce.insertarEmpleados(em);
            out = "{\"result\":\"OK\"}";
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }

    @POST
    @Path("update")
    @Produces("application/json")
    public Response update(
            @FormParam("nombre") @DefaultValue("ert") String nom,
            @FormParam("apellidopa") @DefaultValue("1") String apellidopa,
            @FormParam("apellidoma") @DefaultValue("1") String apellidoma,
            @FormParam("fecha") @DefaultValue("1") String fecha,
            @FormParam("genero") @DefaultValue("1") String genero,
            @FormParam("email") @DefaultValue("1") String email,
            @FormParam("telefono") @DefaultValue("1") String telefono,
            @FormParam("rol") int rol,
            @FormParam("nombreUsuario") @DefaultValue("1") String usuario,
            @FormParam("contrasena") @DefaultValue("1") String contrasena,
            @FormParam("foto") @DefaultValue("1") String foto,
            @FormParam("clave") @DefaultValue("1") String clave
    ) {
        Empleado em = new Empleado();
        EmpleadoControlador ce = new EmpleadoControlador();
        ce.connectDatabase();
        em.setNombre(nom);
        em.setApellidoMa(apellidoma);
        em.setApellidoPa(apellidopa);
        em.setFechaNacimiento(fecha);
        em.setGenero(genero);
        em.setEmail(email);
        em.setTelefono(telefono);
        em.setContrasena(contrasena);
        em.setNombreUsuario(usuario);
        em.setEstatus("Activo");
        em.setRol(rol);
        em.setFoto(foto);
        em.setClaveAcceso(clave);
        String out = null;
        try {
            ce.actualizarEmpleado(em);
            out = "{\"result\":\"OK\"}";
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }

    @POST
    @Path("delete")
    @Produces("application/json")
    public Response eliminar(
            @FormParam("clave") @DefaultValue("ert") String clave
    ) {
        EmpleadoControlador ce = new EmpleadoControlador();
        ce.connectDatabase();
        String out = null;
        try {
            ce.eliminarEmpleado(clave);
            out = "{\"result\":\"OK\"}";
        } catch (Exception e) {
            e.printStackTrace();
            out = "{\"error:\"" + e.toString() + "\"}";
        }
        return Response.status(Response.Status.OK).entity(out).build();
    }
}
