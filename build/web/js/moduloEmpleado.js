/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



var empleados = null;
var empleadoActual = null;

function cargarTablaEmpleado() {
    $.ajax(
            {
                type: "GET",
                url: "api/empleado/getAll",
                async: true
            }).done(
            function (data) {
                console.log(data);
                empleados = data;
                var str = '';
                for (var i = 0; i < empleados.length; i++) {
                    str += '<tr class="table-light">' +
                            '<td>' + empleados[i].nombre + '</td>' +
                            '<td>' + empleados[i].apellidoPa + '</td>' +
                            '<td>' + empleados[i].apellidoMa + '</td>' +
                            '<td>' + empleados[i].fechaNacimiento + '</td>' +
                            '<td>' + empleados[i].email + '</td>' +
                            '<td>' + empleados[i].telefono + '</td>' +
                            '<td>' + empleados[i].nombreUsuario + '</td>' +
                            '<td>' + empleados[i].fechaRegistro + '</td>' +
                            '<td><a href="#" class="text-primary" data-toggle="modal" data-target="#modalEmpleado" \n\ onclick="detalleEmpleado(' + i + ')">\n\
        <i class= "material-icons"></i>Ver Detalle</a>' + '</td></tr>';
                }
                $('#tbEmpleado').html(str);
            }
    );
}

function detalleEmpleado(posicion) {
    empleadoActual = empleados[posicion];
    $('#btnEliminar').show();
    $('#lblTitulo').text("Detalle Empleado");
    $('#txtNombre').val(empleadoActual.nombre);
    $('#txtApePa').val(empleadoActual.apellidoPa);
    $('#txtApeMa').val(empleadoActual.apellidoMa);
    $("#txtGenero > option[value='" + empleadoActual.genero + "']").attr("selected", true);
    $('#txtEmail').val(empleadoActual.email);
    $('#txtTelefono').val(empleadoActual.telefono);
    $('#txtUusario').val(empleadoActual.nombreUsuario);
    $('#txtContrasena').val(empleadoActual.contrasena);
    $('#txtUusario').val(empleadoActual.nombreUsuario);
    $('#imgEmpleado').attr("src", empleadoActual.foto);
    $('#txtFoto').val(empleadoActual.foto);
    $('#txtFecha').val(empleadoActual.fechaNacimiento);
    $("#txtRol > option[value='" + empleadoActual.rol + "']").attr("selected", true);
    $('#txtClave').val(empleadoActual.claveAcceso);
}


function encodeImagetoBase64(element) {
    var file = element.files[0];
    var reader = new FileReader();
    var base64 = "";
    reader.onloadend = function () {
        base64 = reader.result;
        $('#txtFoto').val(base64);
        $('#imgEmpleado').attr("src", base64);
    };
    reader.readAsDataURL(file);
}


function limpiarFormulario() {
  $('#lblTitulo').text("Agregar Empleado");
    empleadoActual = null;
    $('#txtNombre').val("");
    $('#btnEliminar').hide();
    $('#txtApePa').val("");
    $('#txtApeMa').val("");
    $("#txtGenero > option[value='M']").attr("selected", true);
    $('#txtEmail').val("");
    $('#txtTelefono').val("");
    $('#txtUusario').val("");
    $('#txtFecha').val("");
    $('#txtContrasena').val("");
    $('#txtUusario').val("");
    $('#imgEmpleado').attr("src", "");
    $('#txtFoto').val("");
}

function guardarEmpleado() {
    var nombre = $('#txtNombre').val();
    var apellidopa = $('#txtApePa').val();
    var apellidoma = $('#txtApeMa').val();
    var fecha = $('#txtFecha').val();
    var genero = $('#txtGenero').val();
    var email = $('#txtEmail').val();
    var telefono = $('#txtTelefono').val();
    var rol = $('#txtRol').val();
    var nombreUsuario = $('#txtUusario').val();
    var contrasena = $('#txtContrasena').val();
    var foto = $('#txtFoto').val();

    var data = {
        nombre: nombre,
        apellidopa: apellidopa,
        apellidoma: apellidoma,
        fecha: fecha,
        genero: genero,
        email: email,
        telefono: telefono,
        rol: rol,
        nombreUsuario: nombreUsuario,
        contrasena: contrasena,
        foto: foto
    };

    $.ajax(
            {
                type: "POST",
                url: "api/empleado/insert",
                data: data,
                async: true
            }
    ).done(
            function (data) {
                Swal.fire(
  'Empleado agregado correctamente',
  'Presiona el boton',
  'success'
)
                cargarTablaEmpleado();

            });
}

function decidirEmpleado() {
    if (empleadoActual == null) {
        guardarEmpleado();
    } else {
        actualizarEmpleado();

    }
}

function actualizarEmpleado() {
    var nombre = $('#txtNombre').val();
    var apellidopa = $('#txtApePa').val();
    var apellidoma = $('#txtApeMa').val();
    var fecha = $('#txtFecha').val();
    var genero = $('#txtGenero').val();
    var email = $('#txtEmail').val();
    var telefono = $('#txtTelefono').val();
    var rol = $('#txtRol').val();
    var nombreUsuario = $('#txtUusario').val();
    var contrasena = $('#txtContrasena').val();
    var foto = $('#txtFoto').val();
    var clave = $('#txtClave').val();
    var data = {
        nombre: nombre,
        apellidopa: apellidopa,
        apellidoma: apellidoma,
        fecha: fecha,
        genero: genero,
        email: email,
        telefono: telefono,
        rol: rol,
        nombreUsuario: nombreUsuario,
        contrasena: contrasena,
        foto: foto,
        clave: clave
    };

    $.ajax(
            {
                type: "POST",
                url: "api/empleado/update",
                data: data,
                async: true
            }
    ).done(
            function (data) {
                Swal.fire(
  'Empleado actualizado correctamente',
  'Presiona el boton',
  'success'
)
                cargarTablaEmpleado();
            });
}



function eliminarEmpleado(){
    var clave = empleadoActual.claveAcceso;

    console.log(clave);
    var data = {
        clave :clave
    };

    $.ajax(
            {
                type: "POST",
                url: "http://localhost:8084/MongoRest/api/empleado/delete",
                data: data,
                async: true
            }
    ).done(
            function (data) {
                Swal.fire(
  'Empleado eliminado correctamente',
  'Presiona el boton',
  'success'
)
                cargarTablaEmpleado();

            });
}


function buscarEmpleado() {
  var buscar = $('#txtBuscar').val();
    $.ajax(
            {
                type: "GET",
                url: "api/empleado/serch?buscar="+buscar+"",
                async: true
            }).done(
            function (data) {
                console.log(data);
                empleados = data;
                var str = '';
                for (var i = 0; i < empleados.length; i++) {
                    str += '<tr class="table-light">' +
                            '<td>' + empleados[i].nombre + '</td>' +
                            '<td>' + empleados[i].apellidoPa + '</td>' +
                            '<td>' + empleados[i].apellidoMa + '</td>' +
                            '<td>' + empleados[i].genero + '</td>' +
                            '<td>' + empleados[i].fechaNacimiento + '</td>' +
                            '<td>' + empleados[i].email + '</td>' +
                            '<td>' + empleados[i].telefono + '</td>' +
                            '<td>' + empleados[i].nombreUsuario + '</td>' +
                            '<td>' + empleados[i].contrasena + '</td>' +
                            '<td>' + empleados[i].fechaRegistro + '</td>' +
                            '<td><a href="#" class="text-primary" data-toggle="modal" data-target="#modalEmpleado" \n\ onclick="detalleEmpleado(' + i + ')">\n\
        <i class= "material-icons"></i>Ver Detalle</a>' + '</td></tr>';
                }
                $('#tbEmpleado').html(str);
            }
    );
}
